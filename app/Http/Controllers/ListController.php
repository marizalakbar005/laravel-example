<?php

namespace App\Http\Controllers;
use App\Rincian;
use Illuminate\Http\Request;

class ListController extends Controller
{
    //
        public function index ()
        {
            $data = Rincian::all();
            return view('list',compact('data'));
        }
        
        public function create()
        {
            return view('createlist');
        }

        public function store(Request $request)
        {
            // dd($request);
            $data = new Rincian();
            $data->title = $request->title;
            $data->status = 1;
            $data->save();
            
            return redirect('/');
        }

        public function edit($id){
            $data = Rincian::find($id);
            return view('editlist',compact('data'));
        } 

        public function update(Request $request, $id)
        {
            $data = Rincian::find($id);
            $data->title = $request->title;
            $data->save();
            return redirect('/');

        }

        public function check($id)
        {
            $data = Rincian::find($id);
            $data->status = 0;
            $data->save();
            return redirect()->back();
        }

        public function destroy($id)
        {
        $data = Rincian::find($id);
        $data->delete();
        return redirect('/');
    }

    

}

