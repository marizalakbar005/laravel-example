<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table, th, td {
          border: 1px solid black;
          padding: 5px;
        }
        table {
          border-spacing: 15px;
        }
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    
<h2>Create</h2>
    <a href="/create" class="btn btn-primary">Create List</a>
<hr>
<table class="table">
    <thead>
        <th scope="col">Number</th>
        <th scope="col">Title</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach ($data as $item)
            
        <tr>
            <td scope="row">{{$no++}}</td>
            <td>{{$item->title}}</td>
            <td>{{ $item->status == 1 ? 'Uncheck' : 'Check'}}</td>
            <td><form method="POST" action="/destroy/{{$item->id}}">
            @csrf
            <input type="hidden" name="_method" value="DELETE"><a href="/edit/{{$item->id}}" class="btn btn-primary">Edit</a>
            <button type="submit" class="btn btn-primary">Delete</button>
            <a style="{{ $item->status == 0 ? 'display: none' : ''}}" href="{{route('check-list',$item->id)}}" class="btn btn-primary">Check</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>


</body>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>