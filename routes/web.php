<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('list');
// });

Route::get('/check/{id}','ListController@check')->name('check-list');

Route::get('/','ListController@index')->name('index-list');
Route::get('/create','ListController@create')->name('create-list');
Route::POST('/create','ListController@store')->name('store-list');
Route::get('edit/{id}','ListController@edit')->name('edit-list');
Route::patch('/edit/{id}','ListController@update')->name('update-list');
Route::delete('/destroy/{id}','ListController@destroy')->name('destroy-list');



